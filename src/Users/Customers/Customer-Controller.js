const {
        UniqueConstraintError,
        NotFoundError,
        InvalidPropertyError,
        PasswordOrMailError,
        RequiredParameterError
    } = require("../../helpers/errors"),
    {makeHttpError} = require("../../helpers/http-error"),
    {inputDto} = require("../../helpers/ControllerDto/inputDto"),
    {send_message} = require("../../helpers/respons_message");
;

// function customerList(dependencies) {
//     const {
//         database, CustomDataConstructor,
//         CustomerModel, jwt, bcrypt,
//         jwt_config, SALT_WORK_FACTOR
//     } = dependencies;
//     const {private_key_access, private_key_refresh} = jwt_config;
//     const customer_model = CustomerModel({bcrypt, SALT_WORK_FACTOR});
//
//     function promisified_jwt_sign({mail, id}) {
//         return new Promise(((resolve, reject) => {
//             let access_token = jwt.sign({
//                 mail: mail,
//                 id: id.toString(),
//                 exp: Math.floor(Date.now() / 1000) + (60 * 60)
//             }, private_key_access, {algorithm: 'HS384'});
//             let refresh_token = jwt.sign({
//                 mail: mail,
//                 id: id.toString(),
//                 exp: Math.floor(Date.now() / 1000) + (60 * 90)
//             }, private_key_refresh, {algorithm: 'HS384'});
//             if (access_token && refresh_token) {
//                 console.log(jwt.decode(access_token))
//                 resolve({
//                     access_token,
//                     refresh_token
//                 })
//             } else reject("jwt-generation process has failed! ");
//         }))
//     }
//
//     async function compare_pwds(inp_pwd, db_pwd) {
//         //TODO priorityLG solve async problem!!!
//         let result = await bcrypt.compare(inp_pwd, db_pwd);
//         if (result) {
//             return 1;
//         } else throw new PasswordOrMailError();
//     }
//
//
//
//
//
//     return Object.freeze({
//         add,
//         login,
//         update
//         // ,
//         // findByEmail,
//         // findById,
//         // getItems,
//         // remove,
//         // replace,
//         // update
//     })
// }
//
// //TODO to be moved into a separate file dedicated to all validations

//
// exports.Controller_Constructor = (properties) => {
//     const useCases = customerList(properties);
//     let functions = {
//         updateCustomer, signInCustomer, postCustomer
//     }
//
//
//     //wrapper for response sender
//     function createWrapper() {
//         let modified = {};
//         for (let func in functions) {
//             modified[func] = async (ControllerDto, res) => {
//                 if (ControllerDto) console.log("ControllerDto->")
//                 if (res) console.log("res->")
//                 //CHANGED FUNCTION VALUE
//                 try {
//                     let httpRequest = ControllerDto.adaptedHttpRequest;
//                     await functions[func](httpRequest)
//                         .then(data => new ResponseGenerator(null, data).responseSender(res))
//                 } catch (err) {
//                     console.log("EEE", err)
//                     new ResponseGenerator(err, null).errorCatcher(res, err)
//                 }
//             };
//         }
//         console.log("functions -> ", functions["signInCustomer"].toString())
//         return modified;
//     }
//
//
//     function ResponseGenerator(err, response) {
//         this.responseSender = res => (response) ? res
//             .set(response.headers)
//             .status(response.statusCode)
//             .send(response.data) : null;
//         this.errorCatcher = (err) ? (res, err) => res.status(500).json(err.message).end() : null;
//     }
//
//     async function signInCustomer(httpRequest) {
//         console.log("ENTERS")
//         let info = validateInput(httpRequest);
//         // console.log("customer_model", customer_model);
//         try {
//             if (!(info.mail && info.pwd)) {
//                 throw new RequiredParameterError("Login and Password")
//             }
//             const result = await useCases.login(info);
//             return {
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 statusCode: 201,
//                 data: JSON.stringify(result)
//             }
//         } catch (e) {
//             //TODO write separate makeHttpError function which includes all logic for def. message and statusCode!
//             return makeHttpError({
//                 errorMessage: e.message,
//                 statusCode:
//                     e instanceof UniqueConstraintError
//                         ? 409
//                         : e instanceof InvalidPropertyError ||
//                         e instanceof RequiredParameterError
//                         ? 400
//                         : 500
//             })
//         }
//
//     }
//
//     async function updateCustomer(httpRequest) {
//         let info = validateInput(httpRequest, {firstUpper: true});
//         try {
//             const result = await useCases.update(info, httpRequest.checkedAuth);
//
//             return {
//                 headers: {
//                     'Content-Type': 'application/json'
//                 },
//                 statusCode: 201,
//                 data: JSON.stringify(result)
//             }
//         } catch (e) {
//             return makeHttpError({
//                 errorMessage: e.message,
//                 statusCode:
//                     e instanceof UniqueConstraintError
//                         ? 409
//                         : e instanceof InvalidPropertyError ||
//                         e instanceof RequiredParameterError
//                         ? 400
//                         : 500
//             })
//         }
//     }

module.exports = {
    CustomerController,
}

const {expressify} = require("../../helpers/Expressify");

function CustomerController(useCases) {
    async function postCustomer(httpRequest) {
        let info = inputDto(httpRequest);
        // try {
            let res =  await useCases.createCustomer(info);
        return {
            headers: {
                "Content-Type": "application/json"
            },
            statusCode: 201,
            body: {
                ok: true,
                status: 201,
                message: "User created successfully",
                // user,
            },
        };
            // return result;
        // } catch (e) {
        //     return makeHttpError(e);
        // }
    }

    return Object.freeze({
        postCustomer: expressify(postCustomer),

    })
}


// return createWrapper();



