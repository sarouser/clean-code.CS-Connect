const {UniqueConstraintError, ValidationError, PasswordOrMailError, NotFoundError} = require("../../helpers/errors"),
    {send_message} = require("../../helpers/respons_message");

exports.createCustomerList = function (database, {CustomDataConstructor, customer_model}, bcrypt, {jwt, private_key_access, private_key_refresh}) {
    function promisified_jwt_sign({mail, id}) {
        return new Promise(((resolve, reject) => {
            let access_token = jwt.sign({
                mail: mail,
                id: id.toString(),
                exp: Math.floor(Date.now() / 1000) + (60 * 60)
            }, private_key_access, {algorithm: 'HS384'});
            let refresh_token = jwt.sign({
                mail: mail,
                id: id.toString(),
                exp: Math.floor(Date.now() / 1000) + (60 * 90)
            }, private_key_refresh, {algorithm: 'HS384'});
            if (access_token && refresh_token) {
                console.log(jwt.decode(access_token))
                resolve({
                    access_token,
                    refresh_token
                })
            } else reject("jwt-generation process has failed! ");
        }))
    }

    async function compare_pwds(inp_pwd, db_pwd) {
        // const match = await
        //TODO priorityLG solve async problem!!!
        let result = await bcrypt.compare(inp_pwd, db_pwd);
        if (result) {
            return 1;
        } else throw new PasswordOrMailError();
    }

    return Object.freeze({
        add,
        login,
        update
        // ,
        // findByEmail,
        // findById,
        // getItems,
        // remove,
        // replace,
        // update
    })


    // async function find_update(filter, update, res) {
    //     let doc = await Customer.findOneAndUpdate(filter, update, {
    //         returnOriginal: false,
    //         rawResult: true
    //     });
    //     if (!doc.existingUpdated) {
    //
    //     }
    //     console.log("doc");
    //     console.log(doc);
    //     return res(null, doc);
    // }

    async function update(info, checkedAuth) {
        try {
            let res = await customer_model.findOneAndUpdate({_id: checkedAuth.id}, info,
                {returnOriginal: false});
            return send_message().updated();
        } catch (e) {
            throw new NotFoundError();
        }
    }

    async function login(info) {
        //TODO change lookup subject to _id instead of mail!
        const res = await customer_model.findOne({mail: info.mail})
            .catch(err => {
                console.log("ERR of findOne", err)
                throw new PasswordOrMailError();
            });


        if (res) {
            if (await compare_pwds(info.pwd, res.pwd)) {
                let response = {type: res.type, mail: res.mail, id: res._id};
                console.log(res._id);
                //Generate jwts
                response = Object.assign(await promisified_jwt_sign(response));
                return response;
            }
        } else throw new PasswordOrMailError();
    }

    async function add(data) {
        //TODO change new customer creation logic(constructor for vanila js)
        const model = new CustomDataConstructor(data);
        const customer = new customer_model(model);
        try {
            const res = await customer.save();
            return send_message().signed_up();
        } catch (err) {
            if (err.name === 'MongoError' && err.code === 11000) {
                throw new UniqueConstraintError(Object.keys(err.keyPattern)[0], true);
            } else if (err.name === "ValidationError") {
                throw  new ValidationError(err._message)
            }
        }
        // console.log("res", res);
        //TODO change below message to obj(signed_up message)!
    }

//TODO how to keep track of customer services?
// async function get_customer_services(){

}
