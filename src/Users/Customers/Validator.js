const {requiredParam} = require("../../helpers/required-param");
const {isValidEmail} = require("../../helpers/is-valid-email");
const {upperFirst} = require("../../helpers/upper-first");
const {isValidType} = require("../../helpers/isValidUserType");
const {InvalidPropertyError} = require("../../helpers/errors");

//name
// surname
// pwd
// mail
// type
const validatorLogic = (bcrypt = requiredParam("cryptography library is missing!")) => {
    const logic = {
        construct(target, args) {
            let arguments = args[0]
            if (!(arguments["name"] && arguments["surname"] && arguments["mail"] && arguments["type"] && arguments["pwd"]))
                return requiredParam("Invalid customer properties!");
            if (arguments["pwd"].length < 6) throw InvalidPropertyError("Password is too short!");
            isValidEmail(arguments["mail"]);
            [arguments["name"], arguments["surname"]] = upperFirst([arguments["name"], arguments["surname"]]);
            isValidType(arguments["type"]);
            const result = new target(...args);
            return result; // if an object is not returned the construct trap will throw an error
        }
    }
    return logic;
}


module.exports = {
    validatorLogic,
};