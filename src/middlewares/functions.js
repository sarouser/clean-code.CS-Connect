const {RequiredParameterError, ValidationError} = require("../helpers/errors"),
    {private_key_access, private_key_refresh} = require("../../config");
let middleware_functions = {};

function getFunctions({jwt}) {
    function promisified_jwt_sign({mail, _id}) {
        console.log("working middlewares")
        return new Promise(((resolve, reject) => {
            let access_token = jwt.sign({
                mail: mail,
                id: _id.toString(),
                exp: Math.floor(Date.now() / 1000) + (60 * 60)
            }, private_key_access, {algorithm: 'HS384'});
            let refresh_token = jwt.sign({
                mail: mail,
                id: _id.toString(),
                exp: Math.floor(Date.now() / 1000) + (60 * 90)
            }, private_key_refresh, {algorithm: 'HS384'});
            if (access_token && refresh_token) {
                resolve({
                    access_token,
                    refresh_token
                })
            } else reject("jwt-generation process has failed! ");
        }))
    }

    async function produce_jwts(data) {
        console.log("working middlewares")
        const {access_token, refresh_token} = await promisified_jwt_sign(data)
            .catch(err => {
                throw new Error(err);
            });
        return {
            access_token,
            refresh_token
        };

        // catch (e) {
        //     console.log("jwt error->", e)
        // }
        // console.log(access_token);
    }

    // function generate_access_token(data) {
    //     let token = jwt.sign({
    //         mail: data.mail,
    //         id: data.id,
    //         exp: Math.floor(Date.now() / 1000) + (60 * 60)
    //     }, private_key_access, {algorithm: 'HS384'});
    //     return token;
    // }
    //
    // function generate_refresh_token(data) {
    //     let refToken = jwt.sign({
    //         mail: data.mail,
    //         exp: Math.floor(Date.now() / 1000) + (60 * 90)
    //     }, private_key_refresh, {algorithm: 'HS384'});
    //     return refToken;
    // }

    function httpAdapter(req = {}, res, next) {
        console.log("working middlewares")
        req.adaptedHttpRequest = {
            headers: req.headers,
            path: req.path,
            method: req.method,
            pathParams: req.params,
            queryParams: req.query,
            body: req.body,
            checkedAuth: req.checkedAuth || null
        };
        next();
    }


    async function checkAuth(req, res, next) {

        console.log("check auth")
        let headers = req.headers;
        if (!headers) throw new RequiredParameterError("Token", true);
        if (!(headers["access_token"])) {
            console.log("error message", err.message)
            throw new RequiredParameterError("Token", true);
            // let currErr
            // e.log("currErr");
            // console.log(currErr);
            // return res.status(currErr.status).json({message: currErr.message});
        }
        try {
            let verified = await jwt.verify(headers["access_token"],
                private_key_access, {algorithms: "HS384 HS256"});
            req.adaptedHttpRequest.checkedAuth = {
                id: verified.id,
                type: verified.type
            };
            return next();
        } catch (e) {
            next(e);
            // throw new ValidationError(e.message);
        }


        // (err, result) => {
        //     console.log("entered")
        //     //TODO handle errors appropriately
        //     if (err) {
        //         console.log("error message", err.message)
        //         throw new ValidationError(err.message);
        //         // let currErr = new Error401(err.message);
        //         // console.log("currErr");
        //         // console.log(currErr);
        //         // return res.status(currErr.status).json({message: currErr.message});
        //
        //     } else {
        //
        //     }
        // }

        // jwt.verify(headers["access_token"], private_key_access, {algorithms: "HS384 HS256"},
        //     (err, result) => {
        //         console.log("entered")
        //         //TODO handle errors appropriately
        //         if (!(headers || headers["access_token"])) {
        //             console.log("error message", err.message)
        //             throw new RequiredParameterError("Token", true);
        //             // let currErr = new Error401(err.message);
        //             // console.log("currErr");
        //             // console.log(currErr);
        //             // return res.status(currErr.status).json({message: currErr.message});
        //
        //         } else {
        //             ControllerDto.user_id = result.id;
        //             ControllerDto.type = result.type;
        //             console.log(result)
        //             return next();
        //         }
        //     });
    }

    function errorHandlerMiddleware(err, req, res, next) {
        console.log("working middlewares")
        if (err.name === 'TokenExpiredError' || err.name === 'JsonWebTokenError') {
            res.status(401).send({message: err.message});
            // logger.error(err);
            return;
        }
        next();
    }

    return {
        checkAuth, produce_jwts, httpAdapter, errorHandlerMiddleware
    }

    let verify_access = (req, res, next) => {


    };
}

module.exports = {getFunctions}
