//is3Failed-look in db
//isProjectFinished-look in db
//doesOwn-look in db
//Lrestricted-same as above
//jwt- check if exists && still Valid-jwt
//isSpecialist-jwt
//isCustomer-jwt

//path      method      toCheck

//customers/S post        X
//customers/L post        [is3Failed,Lrestricted]
//customers put         [jwt]
//customers del         [jwt]

//specialists/S post        X
//specialists/L post       [is3Failed,Lrestricted]
//specialists put         [jwt]
//specialists del         [jwt]


//spe/service   post        [jwt,isSpecialist]
//spe/service  put         [jwt,isSpecialist,doesOwn]
//spe/service   del         [jwt,isSpecialist,doesOwn]

//spe/project/start (accept by "c")  put         [jwt,isSpecialist]
//spe/project/finish   put         [jwt,isSpecialist]

//project    put         [jwt,isCustomer,isProjectFinished]

//service       post         [jwt,isCustomer] //after that, create a project

//
//
//
//

const {getFunctions} = require("./functions"),
    {ValidationError} = require("../helpers/errors")

function defineFunctions(obj) {


    // const {jwt, customerList} = obj;
    // const {
    //     is_3_failed, access_granted, generate_tokens,
    //     is_project_finished, does_own, produce_jwts,
    //     checkAuth, is_specialist, is_customer
    // } = getFunctions(obj);

    const {produce_jwts, checkAuth} = getFunctions(obj);

    //TODO create messages for all validations!
    return async function ({path, method, headers, body}) {
        if (path === "/login") {
            //login
            //TODO write these 3
            //TODO "Overall logic@" check  first 2, then produce 3. then go to checking (mail ,pass)!
            // is_3_failed();
            // access_granted();
            try {
                let jwts = await produce_jwts(body)
                return jwts;
            } catch (err) {
                throw new Error(err.message);
            }
        } else if ((path === "/costumer" || path === "/specialist")
            && (method === 'PUT' || method === 'DEL')) {
            //
            //[jwt]
            return checkAuth();
        } else if (path === "/specialist/service") {
            if (method === 'PUT' || method === 'DEL') {
                //[jwt,isSpecialist,doesOwn]
            } else if (method === 'POST') {
                //[jwt,isSpecialist]
            }
        } else if ((path === "/specialist/project/start" || path === "/specialist/project/finish")
            && method === 'PUT') {
            //[jwt,isSpecialist]
        } else if (path === "/project" && method === 'PUT') {
            //[jwt,isCustomer,isProjectFinished]
        } else if (path === "/service" && method === 'POST') {
            //[jwt,isCustomer] //after that, create a project
        }
    };

    //TODO write all routes, and then reject all other routes!
}

module.exports = {defineFunctions,}
