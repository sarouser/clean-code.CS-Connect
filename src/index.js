const express = require("express"),
    middleware = require("./middlewares/index"),
    bodyParser = require("body-parser"),
    {handleCustomerRequests} = require("../src/Users/Customers/index"),
    {adaptRequest} = require("../src/helpers/adapt-request"),
    app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

app.use(handleCustomerRequests);

//TODO seperate customer/specialist/project/service endpoints
// app.post('/users/customer', customerController);
// app.post("/login", customerController);
// app.put('/users/customer', customerController);

function customerMiddleware(req, res, next) {
    const httpRequest = adaptRequest(req);
    // if (isValidRequest(httpRequest))
}

function customerController(req, res, next) {
    const httpRequest = adaptRequest(req);
    handleCustomerRequests(httpRequest)
        .then(({headers, statusCode, data}) =>
            res
                .set(headers)
                .status(statusCode)
                .send(data)
        )
        .catch(e => res.status(500).end());
}

let port = 9090;
app.listen(port, () => console.log(`Listening on port ${port}`));
