
const messages = {
    "signed_up" : {
        headers: "application/json",
        success: true,
        message: "You have successfully signed up!"
    },
    "updated" : {
        headers: "application/json",
        success: true,
        message: "You have successfully updated your profile."
    }
};
function send_message(condition){
    return {
        signed_up(){
            return messages["signed_up"];
        },
        updated(){
            return messages["updated"];
        }
    }
    return messages[condition];
}

module.exports = {
    send_message,
}
