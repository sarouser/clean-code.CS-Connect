const {InvalidPropertyError} = require("./errors");
function isValidType(type) {
    if ( type !== "customer" && type !== "specialist")  throw new InvalidPropertyError("Invalid type of the user.");
}

module.exports = {
    isValidType,
}
