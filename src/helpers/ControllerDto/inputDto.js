const {makeHttpError} = require("../http-error");
module.exports = {
    inputDto,
}

/**
 *@inputDto Data Transfer and validation
 * @inputDtoDescription validation and creation of input obj that is appropriate for use cases
 *@inputDtoParam {object}
 *inputDtoSuccess {object}
 */

function inputDto(httpRequest, options) {
    //helper functions
    const helperFunctions = {
        "firstUpper": function (info) {
            info.name &&
            (info.name = info.name.charAt(0).toUpperCase() + info.name.slice(1).toLowerCase());
            info.surname &&
            (info.surname = info.surname.charAt(0).toUpperCase() + info.surname.slice(1).toLowerCase());
        }
    }

    if (!httpRequest) {
        return makeHttpError({
            statusCode: 400,
            errorMessage: 'Bad request. No POST body.'
        })
    }
    let info = httpRequest.body;

    if (typeof httpRequest.body === 'string') {
        try {
            info = JSON.parse(info)
        } catch {
            return makeHttpError({
                statusCode: 400,
                errorMessage: 'Bad request. POST body must be valid JSON.'
            })
        }
    }

    if (options) {
        //"for loop" for further complexity
        for (let i in options) {
            //
            if (options[i] && options.hasOwnProperty(i)) {
                helperFunctions[i](info);
            }
        }
        return info;
    }
    return info;
}