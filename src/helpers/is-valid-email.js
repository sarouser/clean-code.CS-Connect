const {InvalidPropertyError} = require("./errors");

function isValidEmail(address) {
    if (address.length > 320) throw new InvalidPropertyError("Invalid mail.");
    const valid = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/);
    if (!valid.test(address)) throw new InvalidPropertyError("Invalid mail.");
}

module.exports = {
    isValidEmail,
}
