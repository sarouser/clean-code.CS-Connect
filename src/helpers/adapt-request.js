function adaptRequest(req = {}) {
    return Object.freeze({
        headers: req.headers,
        path: req.path,
        method: req.method,
        pathParams: req.params,
        queryParams: req.query,
        body: req.body,
        checkedAuth : req.checkedAuth || null
    })
}

module.exports = {
    adaptRequest,
}
