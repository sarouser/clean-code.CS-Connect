const {makeHttpError} = require("../helpers/http-error");

function expressify(controller) {

    return async (req, res) => {
        const httpRequest = {
            body: req.body,
            query: req.query,
            params: req.params,
            ip: req.ip,
            method: req.method,
            path: req.path,
            headers: {
                "Content-Type": req.get("Content-Type"),
                Referer: req.get("referer"),
                "User-Agent": req.get("User-Agent"),
            },
        }

        // try {
            let httpResponse = await controller(httpRequest);
            if (httpResponse.headers) {
                res.set(httpResponse.headers);
            }
            res.type("json");
            const body = {
                ...httpResponse.body,
            };
            res.status(httpResponse.statusCode).send(body);
        // } catch (e) {
        //     let resp = makeHttpError(e)
        //     res.status(resp.statusCode || 500).set(resp.headers).send(resp.data);
        // }
    };
}

module.exports = {
    expressify,
}