class UniqueConstraintError extends Error {
    constructor(value, registration = false) {
        registration ? super(`Mail address already exists!`)
            : super(`${value} must be unique!`);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, UniqueConstraintError)
        }
    }
}

class PasswordOrMailError extends Error {
    constructor() {
        super();
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, PasswordOrMailError)
        }
        this.message = 'Password or/and Mail is wrong!';
    }
}

class NotFoundError extends Error {
    constructor(message) {
        super();
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, NotFoundError)
        }
        this.message = message;
    }
}

class RequiredParameterError extends Error {
    constructor(param, token = false) {
        token ? super(`${param} must be presented!`)
            : super(`${param} can not be null or undefined.`);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, RequiredParameterError)
        }
    }
}


class ValidationError extends Error {
    constructor(message) {
        super(message);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, ValidationError)
        }
        this.name = "ValidationError";
    }
}

class InvalidPropertyError extends Error {
    constructor(msg) {
        super(msg)
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, InvalidPropertyError)
        }
    }
}

class PropertyRequiredError extends ValidationError {
    constructor(property) {
        super("No property: " + property);
        this.name = "PropertyRequiredError";
        this.property = property;
    }
}

module.exports = {
    PasswordOrMailError,
    UniqueConstraintError,
    ValidationError,
    InvalidPropertyError,
    PropertyRequiredError,
    RequiredParameterError,
    NotFoundError
}
